<?php
/* Sudoku Generator */
//I would do a namespace but they never work for me.
class sudoku //making sure it doesn't conflict with other functions and stuff
{
	function generation($level = 0, $length = 4) { //the main function of the class! it holds the logic to make the sudoku
		for ($line = 0; $line < $length; $line++) //does this stuff for all the lines, doing this instead of by square. This is easier I think...
		{
			for ($num = 0; $num < $length; $num++) //this "for" is to generate the array of numbers needed for each line to pick from...
			{
				$numbers[$num] = $num+1; //because arrays start at 0 and sudokus start at 1 we cannot use $num raw! Well, we could but that would make things more difficult later...
			} //end num for
			for ($cell = 0; $cell < $length; $cell++) //this "for" holds the logic to make sure each cell gets a number that doesn't conflict with other lines, columns or squares
			{
				do //this "while" loops until the number is no longer the same as others in it's column or square
				{
					for ($i = 0; $i < $line; $i++) //this "for" counts done from the line number to 1
					{
						if ($sudoku[$line][$cell] != $sudoku[$i][$cell]) //Check against the "for"'s variable to see if it isn't in the column
						{
							$same = FALSE; //If it isn't set same to FALSE
						} else {
							$same = TRUE; //If it is set same to TRUE
						} //end if
					} //end for
					if ($same == TRUE)
					{
						$item = mt_rand(0,count($numbers)-1); //randomly get an item from the number array, the count($numbers) is there because the array gets shorter with every cell to make there be less need to logic checking! :D
						$sudoku[$line][$cell] = $numbers[$item]; //make cell equal that item of the number array
					}
				} while ($same); //end do while
				sudoku::shortenArray($numbers, $item); //get rid of that item from the array so future cells cannot have it (less logic checking!)
			} //end cell for
		} //end line for
		return $sudoku; //return the sudoku array
	} //end generation function
	function shortenArray(&$array, $var) { //this is for making it so I do not have to logic check the numbers perline, it deletes the number from the array. I use reference instead of the regualar way for the array because there is no need to pass it back and forth
		for (; $var < count($array)-1; $var++) //this "for" is to count up from the var to be deleted to the one before the end of the array (don't need to count to the last one as it is going to be deleted anyways
		{
			$array[$var] = $array[$var+1]; //set the item in the array to one higher in the array effectively deleting that number from the array
		}//end var for
		array_pop($array); //delete the last item in the array so we do not have duplicates of the last number in the array
	} //end shortenArray function
	function display($sudoku, $return = FALSE) { //this function displays the sudoku in a table (one of the few uses for a table!). This is only a basic display function, it is not needed for the sudoku class to function. $return is to choose if it is returned from the function or just displayed outright from the function
		switch ($return) //chose if this function should return the table or display it
		{
		case TRUE: //start TRUE case
			$table = "<table border='1'>"; //start table
			for ($line = 0; $line < count($sudoku); $line++) //for every line
			{
				$table .= "<tr>"; //start row
				for ($cell = 0; $cell < count($sudoku[$line]); $cell++) //for every cell
				{
					$table .= "<td>".$sudoku[$line][$cell]."</td>"; //fill a table cell
				}//end cell for
				$table .= "</tr>"; //end row
			}//end line for
			$table .= "</table>"; //end table
			return $table; //return the table
		break; //end TRUE case
		case FALSE: //start FALSE case
			echo "<table border='1'>"; //start table
			for ($line = 0; $line < count($sudoku); $line++) //for every line
			{
				echo "<tr>"; //start row
				for ($cell = 0; $cell < count($sudoku[$line]); $cell++) //for every cell
				{
					echo "<td>".$sudoku[$line][$cell]."</td>"; //fill a table cell
				}//end cell for
				echo "</tr>"; //end row
			}//end line for
			echo "</table>"; //end table
		break; //end FALSE case
		} //end switch
	}//end display function
}
echo sudoku::display(sudoku::generation(0, 4), TRUE); //make and display a sudoku (or what it creates thus far)
?>